#!/bin/bash

read -p "Enter a Number: " num

count=0
isPrimeFunction() {
    for((i=1; i<=$num; i++)); do 
        if [ $((num%i)) -eq 0 ]; then
            count=$((count+1))
        fi
    done
}

isPrimeFunction

if [ $count -eq 2 ]; then
    echo "This is a Prime Number."
else
    echo "This is not a Prime Number"
fi

