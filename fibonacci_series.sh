#!/bin/bash

read -p "Enter a number greater than 3: " num

if [ $num -le 3 ]; then
    exec bash "$0"
fi

first_fib=0
second_fib=1

nthFibonacci() {
    for((i=3; i<=$num; i++)); do
        local temp=$second_fib
        second_fib=$((second_fib+first_fib))
        first_fib=$temp
    done
}

nthFibonacci

echo $second_fib


    

