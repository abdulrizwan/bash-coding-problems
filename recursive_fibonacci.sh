#!/bin/bash

read -p "Enter a Number greater than 3: " num

if [ $num -le 3 ]; then
    exec bash "$0"
fi

nthFibonacciRecursiveFunction() {
    if [ $1 -le 1 ]; then
        echo $1
    else
        echo $(( $(nthFibonacciRecursiveFunction $(($1-1))) + $(nthFibonacciRecursiveFunction $(($1-2))) ))
    fi
}

result=$(nthFibonacciRecursiveFunction $num)

echo "$num-th Fibonacci number is: $result"