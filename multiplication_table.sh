#!/bin/bash

read -p "Enter a number: " num

multiplicationTable() {
    for i in {1..10}; do
        echo "$num X $i = $(($num * $i))"
    done
}

multiplicationTable